Drofile Make
=========

Drofile Drupal make file

v7.x-1.0

Drofile is a full D7 install profile, providing a complete Drupal setup & basic config to reduce time spent doing repetitive install & setup tasks common to all Drupal 7 sites.

It includes a comprehensive set of modules alongside a default admin theme (Adminimal) & base theme (Drubath)

Docs:
----

Run the following commands from commandline (unix/linux) **inside the directory you will want to create the new site**.

**Note:** [Drush](http://docs.drush.org/en/master/install/) needs to be installed on your machine!
``````
git clone https://gitlab.com/drofile/drofile-make.git
cd drofile-make
mv drupal-org-core.make ../
cd ..
rm -rf drofile-make
drush make drupal-org-core.make
``````


One-line command ;)

`git clone https://gitlab.com/drofile/drofile-make.git && cd drofile-make && mv drupal-org-core.make ../ && cd .. && rm -rf drofile-make && drush make drupal-org-core.make`

